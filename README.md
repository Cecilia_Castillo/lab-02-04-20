# lab 02-04-20

El objetivo de este laboratorio es evaluar: estructuras de condicionales y de bucles, listas, tuplas, funciones
y archivos, por lo que cada uno de los ejercicios debe contemplar cada uno de esos elementos en caso de ser
necesarios.